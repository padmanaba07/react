const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const { check, validationResult } = require('express-validator');

const User = require('../../models/User');
const Posts = require('../../models/Posts');
const Profile = require('../../models/Profile');

// @route   POSTS api/pots
// @desc    Create a post
// @access  Private  
router.post('/', [auth, [
    check('text','Text is Required').not().isEmpty(),

]], async (req,res) => {
    const errors = validationResult(req);

    if(!errors.isEmpty())
    {
        return res.status(400).json({ errors: errors.array() });
    }

    try {
        const user = await User.findById(req.user.id).select('-password');
     const newPost = new Posts({
        text: req.body.text,
        name: user.name,
        avatar: user.avatar,
        user: req.user.id
        });
    const post = await newPost.save();

    res.json(post);
    } catch (err) {
        console.error(err.message);
        res.status(500).json('Server Error');
    }
    
});

// @route   GET api/posts
// @desc    Get all posts
// @access  Private  

router.get('/', auth, async (req,res) => {
    try {
        const posts = await Posts.find().sort({ date: -1});
        res.json(posts);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});

// @route   GET api/posts/:id
// @desc    Get posts by id
// @access  Private  

router.get('/:id', auth, async (req,res) => {
    try {
        const posts = await Posts.findById(req.params.id);

        if(!posts)
        {
            return res.status(400).json({ msg: 'no post Found'});
        }
        res.json(posts);
    } catch (err) {
        console.error(err.message);
        if(err.kind ==='ObjectId')
        {
            return res.status(400).json({ msg: 'no post Found'});   
        }
        res.status(500).send('Server Error');
    }
});

// @route   DELETE api/posts
// @desc    Get all posts
// @access  Private  

router.delete('/:id', auth, async (req,res) => {
    try {
        const posts = await Posts.findById(req.params.id);

        if(!posts)
        {
            return res.status(400).json({ msg: 'no post Found'});
        }

        // Check User
        if(posts.user.toString() !== req.user.id)
        {
            return res.status(401).json( { msg: 'User not Authorized'});
        }

        await posts.remove();
        res.json({ msg: 'Post Removed Successfully'});
    } catch (err) {
        console.error(err.message);
        if(err.kind ==='ObjectId')
        {
            return res.status(400).json({ msg: 'no post Found'});   
        }
        res.status(500).send('Server Error');
    }
});

// @route   PUT api/posts/like/:id
// @desc    Like a post
// @access  Private  

router.put('/like/:id', auth, async (req,res) => {
    try {
        const posts = await Posts.findById(req.params.id);

        // Check if the post is already liked
        if(posts.likes.filter(like => like.user.toString() === req.user.id).length > 0) {
            return res.status(400).json({ msg: "Post already liked"} );
        }
        posts.likes.unshift({ user: req.user.id });
        await posts.save();
        res.json(posts.likes);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
        
    }
});

// @route   PUT api/posts/unlike/:id
// @desc    UnLike a post
// @access  Private  

router.put('/unlike/:id', auth, async (req,res) => {
    try {
        const posts = await Posts.findById(req.params.id);

        // Check if the post is already liked
        if(posts.likes.filter(like => like.user.toString() === req.user.id).length === 0) {
            return res.status(400).json({ msg: "Post has not yet been liked"} );
        }
        // get Remove index
        const removeIndex = posts.likes.map(like => like.user.toString()).indexOf(req.user.id);
        
        posts.likes.splice(removeIndex, 1);
        await posts.save();
        res.json(posts.likes);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
        
    }
});

// @route   POSTS api/posts/comment/:id
// @desc    Create a post
// @access  Private  
router.post('/comment/:id', [auth, [
    check('text','Text is Required').not().isEmpty(),

]], async (req,res) => {
    const errors = validationResult(req);

    if(!errors.isEmpty())
    {
        return res.status(400).json({ errors: errors.array() });
    }

    try {
        const user = await User.findById(req.user.id).select('-password');
        const posts = await Posts.findById(req.params.id);

     const newComment = {
        text: req.body.text,
        name: user.name,
        avatar: user.avatar,
        user: req.user.id
        };
        posts.comments.unshift(newComment);
        
        await posts.save();

    res.json(posts.comments);
    } catch (err) {
        console.error(err.message);
        res.status(500).json('Server Error');
    }
    
});

// @route   DELETE api/posts/comment/:id
// @desc    Delete a post
// @access  Private  
router.delete('/comment/:id/:comment_id', auth, async (req,res) => {
    try {
        const post = await Posts.findById(req.params.id);

        // Get The comment from the post
        const comment = post.comments.find(comment => comment.id === req.params.comment_id);

        if(!comment)
        {
            return res.status(404).json({ msg: 'Comment Not Found'} );
        }

        // Check User
        if(comment.user.toString() !== req.user.id)
        {
            return res.status(401).json({ msg: 'User is not Authorized'} );
        }

        const removeIndex = post.comments.map(comment => comment.user.toString()).indexOf(req.user.id);

        post.comments.splice(removeIndex,1);
        await post.save();

        res.json(post.comments);
        
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server Error');
    }
});

module.exports = router;