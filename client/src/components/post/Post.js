import React, {Fragment, useEffect} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import Spinner from '../layout/Spinner';
import  PostItem  from '../posts/PostItem';
import { getPost } from '../../actions/post'

const Post = ({getPost, post: {post, loading}, match}) => {
    useEffect(() => {
        getPost(match.params.id)
    },[getPost]);
    return loading || post === null ? <Spinner /> : <Fragment>
        <PostItem post = {post} showactions={false}/>
    </Fragment>
}

Post.propTypes = {
    getPost:PropTypes.func.isRequired,
    post:PropTypes.object.isRequired,
}
const mapstateToProps = state => ({
    post: state.post
})
export default connect(mapstateToProps,{getPost})(Post)
